# Synopsis


This channel contains my personally created packages. If they are not
pushed to the Guix's upstream, they will be some time, or they shouldn't.


# Contents


The following packages are included here:

### admin.scm

Admin software, currently only one package included:
  - shepherd-master (shepherd, pinned to commit `7c380590164ea8ee40de46059d07e08a48963577`)

### music.scm

Contains software related to music.

Packages included:
  - mixxx (superseded by the upstream)
  - python-mopidy
  - python-mopidy-mpd
  - python-mopidy-bandcamp

### news.scm

Some utilities for getting/sending news.

`sinntp` is a simple collection of cli utilities for NNTP - very useful for scripting.

In the future, `lieferhund` will also be included here.

Packages included:
  - sinttp
  
### python-web.scm

Some python packages, mostly generated with `guix import pypi`.

Packages included:
  - python-expiringdict
  - python-magnet2torrent

### python-xyz.scm

Some dependencies.

### fonts.scm


Some fonts for daily use.


Termsyn, in three different outputs: pcf, psf and otf. Tbh the otf one renders
in a very ugly way, at least for me.


And since Pango 1.44 doesn't support pcf fonts anymore - a TTF font to save
the day - Terminus.


Packages included:
  - font-termsyn
  - font-terminus-ttf

### vnc.scm

Vnc utilities/servers:

  - libaml
  - libneatvnc
  - wayvnc (a VNC server for Wayland!)

### perl.scm

Perl libraries:

  - perl-linux-fd


### wm.scm

Just sway 1.6.2

### commencement-no32.scm


An attempt to use x86 mesboot to bootstrap a system accepting mostly only
x86_64 syscalls; since it works under a normal kernel and x86_64 support for
mesboot is going to be here soon, this won't make much sense in the future.


The idea would be patching your own guix channel, and to let
`guix pull --no-substitutes` bootstrap your system. This fails
at the moment gnu make tries to syscall `execve` and fails (on
some kernels - maybe others are lucky).

Not guaranteed to work - and even if it does, it was futile in my case.


Packages included:
  - binutils-mesboot0-no32
  - gawk-mesboot-no32
  - gnu-make-mesboot-no32
