;;
;; Copyright © kitzman <kitzman @ disroot . org>
;;

(define-module (kitzman packages music)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix profiles)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system qt)
  #:use-module (guix build-system python)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages benchmark)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages video)
  #:use-module (gnu packages music)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages python)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-compression)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (kitzman packages python-xyz)
  #:use-module (gnu packages gstreamer))

;;
;; Mixxx (SUPERSEDED)
;;

(define-public mixxx
  (package
    (name "mixxx")
    (version "2.3.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/mixxxdj/mixxx")
             (commit version)))
       (file-name (git-file-name name version))
       (patches
        (search-patches "mixxx-link-qtscriptbytearray-qtscript.patch"
                        "mixxx-system-googletest-benchmark.patch"))
       (sha256
        (base32 "0zrmy97lk0xdfnlvygshr8vradypmnypki3s1mhc296xhq96n6rm"))
       (modules '((guix build utils)))
       (snippet
        ;; Delete libraries that we already have or don't need.
        ;; TODO: try to unbundle more (see lib/).
        `(begin
           (let ((third-parties '("apple" "benchmark" "googletest" "hidapi"
                                  "libebur128")))
             (with-directory-excursion "lib"
               (map (lambda (third-party)
                      (delete-file-recursively third-party))
                    third-parties)))
           #t))))
    (build-system qt-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         ;; Tests need a running X server.
         (add-before 'check 'prepare-x-for-test
           (lambda _
             (system "Xvfb &")
             (setenv "DISPLAY" ":0")))
         (add-after 'install 'wrap-executable
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (faad2 (assoc-ref inputs "faad2")))
               (wrap-program (string-append out "/bin/mixxx")
                 `("LD_LIBRARY_PATH" ":" prefix
                   ,(list (string-append faad2 "/lib"))))))))))
    (native-inputs
     (list benchmark
           googletest
           python-wrapper
           qttools-5
           xorg-server-for-tests))
    (inputs
     (list bash-minimal
           chromaprint
           faad2
           ffmpeg
           fftw
           flac
           glu
           hidapi
           jack-1
           lame
           libdjinterop
           libebur128
           libid3tag
           libkeyfinder
           libmad
           libmp4v2
           libmodplug
           libsndfile
           libshout
           ;; XXX: Mixxx complains the libshout-idjc package suffers from bug
           ;; lp1833225 and refuses to use it.  Use the bundle for now.
           ;; libshout-idjc
           libusb
           libvorbis
           lilv
           mp3guessenc
           openssl
           opusfile
           portaudio
           portmidi
           protobuf
           qtbase-5
           qtdeclarative-5
           qtkeychain
           qtscript
           qtsvg-5
           qtx11extras
           rubberband
           soundtouch
           sqlite
           taglib
           upower
           vamp
           wavpack))
    (home-page "https://mixxx.org/")
    (synopsis "DJ software to perform live mixes")
    (description "Mixxx is a DJ software.  It integrates the tools DJs need to
perform creative live mixes with digital music files.")
    (license license:gpl2+)))

;;
;; Mopidy
;;

(define-public python-mopidy
  (package
    (name "python-mopidy")
    (version "3.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Mopidy" version))
       (sha256
        (base32 "013hqfd80bjr0x6rx76fbfayn9yflx4q5ravji1sqh1w3alim7cf"))))
    (build-system python-build-system)
    (propagated-inputs
     (list python-pykka python-requests python-tornado nss-certs
           python-gst gstreamer gst-plugins-base gst-plugins-good
           gst-plugins-bad gst-plugins-ugly))
    (native-inputs
     (list python-black
           python-check-manifest
           python-flake8
           python-flake8-black
           python-flake8-bugbear
           python-flake8-isort
           python-isort
           python-mypy
           python-pep8-naming
           python-pygraphviz
           python-pytest
           python-pytest-cov
           python-responses
           python-sphinx
           python-sphinx-rtd-theme
           python-tox
           python-types-requests
           python-types-setuptools))
    (arguments
     `(#:tests? #f))
    (home-page "https://mopidy.com/")
    (synopsis "Mopidy is an extensible music server written in Python")
    (description "Mopidy is an extensible music server written in Python")
    (license license:asl2.0)))

(define-public python-mopidy-mpd
  (package
    (name "python-mopidy-mpd")
    (version "3.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Mopidy-MPD" version))
       (sha256
        (base32 "18wxjkzbm56a4jazgjhghla3ay32dpy07xkkk71fxjk5mnpwm6x1"))))
    (build-system python-build-system)
    (propagated-inputs
     (list python-pykka))
    (native-inputs
     (list python-mopidy
           python-black
           python-check-manifest
           python-flake8
           python-flake8-black
           python-flake8-bugbear
           python-isort
           python-pytest
           python-pytest-cov))
    (home-page "https://github.com/mopidy/mopidy-mpd")
    (synopsis "Mopidy extension for controlling Mopidy from MPD clients")
    (description "Mopidy extension for controlling Mopidy from MPD clients")
    (license license:asl2.0)))

(define-public python-mopidy-bandcamp
  (package
    (name "python-mopidy-bandcamp")
    (version "1.1.5")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Mopidy-Bandcamp" version))
       (sha256
        (base32 "012w2iw09skayskbswp5dak0mp5xf3p0ld90lxhh8rczw9q763y2"))))
    (build-system python-build-system)
    (propagated-inputs (list python-pykka))
    (native-inputs (list python-mopidy))
    (home-page "https://github.com/impliedchaos/mopidy-bandcamp")
    (synopsis "Backend for listening to bandcamp")
    (description "Backend for listening to bandcamp")
    (license license:expat)))
