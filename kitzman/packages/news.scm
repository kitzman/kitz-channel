;;
;; Copyright © kitzman <kitzman @ disroot . org>
;;

(define-module (kitzman packages news)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages python)
  #:use-module ((guix licenses) #:prefix license:))

(define-public sinntp
  (package
    (name "sinntp")
    (version "1.6")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append
         "https://github.com/jwilk/sinntp/releases/download/"
         version "/sinntp-" version ".tar.gz"))
       (sha256
        (base32 "1sk02pilc948zppskrj54j9mfrha1nlqjyi74g7a52s0vxncq6im"))))
    (build-system trivial-build-system)
    (inputs `(("python" ,python)))
    (native-inputs `(("coreutils" ,coreutils)
                     ("tar" ,tar)
                     ("gzip" ,gzip)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder (begin
                   (use-modules (guix build utils)
                                (srfi srfi-26))

                   (let* ((coreutils (assoc-ref %build-inputs "coreutils"))
                          (source    (assoc-ref %build-inputs "source"))
                          (tar       (assoc-ref %build-inputs "tar"))
                          (gzip      (assoc-ref %build-inputs "gzip"))

                          (output (assoc-ref %outputs "out"))
                          (bin    (string-append output "/bin"))
                          (share  (string-append output "/share/sinntp"))
                          (manpages    (string-append output "/share/man")))

                     (setenv "PATH" (string-append gzip "/bin"))
                     (invoke (string-append tar "/bin/tar") "xvf"
                             source)
                     (chdir ,(string-append "sinntp-" version))
                     
                     (mkdir-p bin)
                     (mkdir-p share)
                     (mkdir-p manpages)

                     (for-each (cut install-file <> manpages)
                               (find-files "doc/manpages"))
                     (for-each (cut install-file <> share)
                               '("./sinntp" "./utils.py" "./plugins.py"))
                     (for-each (cut install-file <> bin)
                               '("./nntp-push" "./nntp-pull" "./nntp-list" "./nntp-get"))
                     (invoke (string-append coreutils "/bin/ln") "-s"
                             (string-append share "/sinntp")
                             (string-append bin "/sinntp"))
                     #t))))
    (home-page "http://jwilk.net/software/sinntp")
    (synopsis "Tiny, non-interactive NNTP client")
    (description
     "sinntp is a tiny non-interactive NNTP client.

The following executables are included: sinntp, nntp-list,
nntp-push, nntp-pull, nntp-get.")
    (license license:gpl2)))
