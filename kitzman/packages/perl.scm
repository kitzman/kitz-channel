;;
;; Copyright © 2022 kitzman <kitzman @ disroot . org>
;;

(define-module (kitzman packages perl)
  #:use-module (srfi srfi-1)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system perl)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages perl-check))

(define-public perl-linux-fd
  (package
    (name "perl-linux-fd")
    (version "0.011")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "mirror://cpan/authors/id/L/LE/LEONT/Linux-FD-" version
                    ".tar.gz"))
              (sha256
               (base32
                "0mkklc7z1zhl0sm0kb66fcq0csdf17lpgzr6av9hxjs4fva7kdbb"))))
    (build-system perl-build-system)
    (native-inputs `(("perl-module-build" ,perl-module-build) ("perl-test-exception" ,perl-test-exception)))
    (propagated-inputs `(("perl-sub-exporter" ,perl-sub-exporter)))
    (home-page "https://metacpan.org/release/Linux-FD")
    (synopsis "Linux specific special filehandles")
    (description "Linux specific special filehandles")
    (license license:perl-license)))
