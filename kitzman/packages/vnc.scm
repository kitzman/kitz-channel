;;
;; Copyright © 2022 kitzman <kitzman @ disroot . org>
;;

(define-module (kitzman packages vnc)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system meson)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages image)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages man)
  #:use-module (gnu packages compression)
  #:use-module ((guix licenses) #:prefix license:))

(define-public libaml
  (package
    (name "libaml")
    (version "0.2.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/any1/aml")
                    (commit (string-append "v" version))))
              (file-name (git-file-name "aml" (string-append "v" version)))
              (sha256
               (base32
                "1cd1y3awr9jajgax286gjiysjjx3sxjh3bcirvkjnaxkvcmn662s"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config))
    (home-page "https://github.com/any1/aml")
    (synopsis "Another/Andri's main loop")
    (description "The @code{aml} library provides an event loop for C, for Linux,
with the following features:
@enumerate
@item fd event handlers
@item timers and tickers
@item signal handlers
@item idle dispatch callbacks
@item thread pool support
@end enumerate
")
    (license license:isc)))

(define-public libneatvnc
  (package
    (name "libneatvnc")
    (version "0.5.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/any1/neatvnc")
                    (commit (string-append "v" version))))
              (file-name (git-file-name "neatvnc" (string-append "v" version)))
              (sha256
               (base32
                "15kk2bnwm5vywh7ha37dcirpxdwqdw11zdvdczg0w1gwvz7j3xir"))))
    (build-system meson-build-system)
    (native-inputs (list pkg-config))
    (inputs
     (list libdrm libglvnd libxkbcommon pixman libaml gnutls libjpeg-turbo zlib))
    (home-page "https://github.com/any1/neatvnc")
    (synopsis "Lightweight VNC server library")
    (description "NeatVNC is a lightweight VNC server library, and partially
supporting authentication, SSH tunneling, and ZRLE or Tight encoding.")
    (license license:isc)))

(define-public wayvnc
  (package
    (name "wayvnc")
    (version "0.5.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/any1/wayvnc")
                    (commit (string-append "v" version))))
              (file-name (git-file-name "wayvnc" (string-append "v" version)))
              (sha256
               (base32
                "07jkhk2q18h7agvxbyg49n3mfn4prhh7k419a1s59pkqmd0a9qpw"))))
    (build-system meson-build-system)
    (native-inputs
     (list pkg-config scdoc))
    (inputs
     (list libaml libneatvnc zlib libjpeg-turbo gnutls libdrm pixman libglvnd libxkbcommon wayland))
    (home-page "https://github.com/any1/wayvnc")
    (synopsis "VNC server for wlroots-based Wayland compositors")
    (description "This is a VNC server for wlroots-based Wayland compositors (no_entry Gnome and KDE are not supported). It attaches to a running Wayland session, creates virtual input devices, and exposes a single display via the RFB protocol. The Wayland session may be a headless one, so it is also possible to run wayvnc without a physical display attached.")
    (license license:isc)))
