;;
;; Copyright © 2022 kitzman <kitzman @ disroot . org>
;;

(define-module (kitzman packages admin)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages man)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages guile)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:))

(define-public shepherd-master
  (package
    (name "shepherd-master")
    (version "7c380590164ea8ee40de46059d07e08a48963577")
    (source (origin
              (method url-fetch)
              (uri
               (string-append
                "https://git.savannah.gnu.org/cgit/shepherd.git/snapshot/shepherd-"
                version ".tar.gz"))
              (sha256
               (base32
                "0q0jskdc9qgjhr4p33yfh6jylhda5iaylza3k6yxzdfybx0439vm"))
              (modules '((guix build utils)))))
              ;; (snippet
              ;;  '(begin
              ;;     ;; Build with -O1 to work around <https://bugs.gnu.org/48368>.
              ;;     ()
              ;;     (substitute* "Makefile.in"
              ;;       (("compile --target")
              ;;        "compile -O1 --target"))))))
    (build-system gnu-build-system)
    (arguments
     '(#:configure-flags '("--localstatedir=/var")
       #:make-flags '("GUILE_AUTO_COMPILE=0")))
    (native-inputs
     (list pkg-config
           autoconf
           automake
           gettext-minimal
           help2man
           texinfo
           guile-3.0))
    (inputs
     ;; ... and this is the one that appears in shebangs when cross-compiling.
     (list guile-3.0
           ;; The 'shepherd' command uses Readline when used interactively.  It's
           ;; an unusual use case though, so we don't propagate it.
           guile-readline))
    (synopsis "System service manager")
    (description
     "The GNU Shepherd is a daemon-managing daemon, meaning that it supervises
the execution of system services, replacing similar functionality found in
typical init systems.  It provides dependency-handling through a convenient
interface and is based on GNU Guile.")
    (license license:gpl3+)
    (home-page "https://www.gnu.org/software/shepherd/")))
