;;
;; Copyright © kitzman <kitzman @ disroot . org>
;;

(define-module (kitzman packages python-xyz)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix profiles)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system python)
  #:use-module (gnu packages python)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-compression)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages gstreamer))

(define-public python-types-urllib3
  (package
    (name "python-types-urllib3")
    (version "1.26.9")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "types-urllib3" version))
       (sha256
        (base32 "0b2j7zpqr8jdvr6bx6mw3m9wr3b7hw2pz0dl6hc2nj1pg22x9lmb"))))
    (build-system python-build-system)
    (home-page "https://github.com/python/typeshed")
    (synopsis "Typing stubs for urllib3")
    (description "Typing stubs for urllib3")
    (license license:asl2.0)))

(define-public python-types-setuptools
  (package
    (name "python-types-setuptools")
    (version "57.4.9")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "types-setuptools" version))
       (sha256
        (base32 "1dahyi6vw823k034lavr7k7y8x3fi1zqh6f79yzf9qgq8i3zfvjk"))))
    (build-system python-build-system)
    (home-page "https://github.com/python/typeshed")
    (synopsis "Typing stubs for setuptools")
    (description "Typing stubs for setuptools")
    (license license:asl2.0)))

(define-public python-types-requests
  (package
    (name "python-types-requests")
    (version "2.27.9")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "types-requests" version))
       (sha256
        (base32 "1sl758jf6h0c3zgzc0r23sql014k6ardmpzgjaa975yj6i2rfs3k"))))
    (build-system python-build-system)
    (propagated-inputs (list python-types-urllib3))
    (home-page "https://github.com/python/typeshed")
    (synopsis "Typing stubs for requests")
    (description "Typing stubs for requests")
    (license license:asl2.0)))

(define-public python-pykka
  (package
    (name "python-pykka")
    (version "3.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pykka" version))
       (sha256
        (base32 "0qrjiam9b1q9ifwab8jdxdqdjw005a9ghgl0h83vrfip9w5hbnhm"))))
    (build-system python-build-system)
    (propagated-inputs (list python-importlib-metadata))
    (home-page "https://github.com/jodal/pykka")
    (synopsis "Pykka is a Python implementation of the actor model")
    (description "Pykka is a Python implementation of the actor model")
    (license license:asl2.0)))

(define-public python-flake8-isort
  (package
    (name "python-flake8-isort")
    (version "4.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "flake8-isort" version))
       (sha256
        (base32 "05r7z0j9rqgy0a9261bhisairrz6w8hy5hy5kf2mhvhfnx53056q"))))
    (build-system python-build-system)
    (propagated-inputs (list python-flake8 python-isort python-testfixtures))
    (native-inputs (list python-pytest-cov))
    (home-page "https://github.com/gforcada/flake8-isort")
    (synopsis "Plugin for flake8 that integrates isort")
    (description "flake8 plugin that integrates isort")
    (license #f)))

(define-public python-flake8-black
  (package
    (name "python-flake8-black")
    (version "0.2.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "flake8-black" version))
       (sha256
        (base32 "08px19jv9pqkpcmrlssyaw7wh4252np61flizjhk3x5z3kyip1x7"))))
    (build-system python-build-system)
    (propagated-inputs (list python-black python-flake8 python-toml))
    (home-page "https://github.com/peterjc/flake8-black")
    (synopsis "Plugin for flake8 to call black as a code style validator")
    (description "flake8 plugin to call black as a code style validator")
    (license license:expat)))
