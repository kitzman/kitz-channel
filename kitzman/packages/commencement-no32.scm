;;
;; Copyright © 2021 kitzman <kitzman @ disroot.org>
;;
;; An attempt to use x86 mesboot to bootstrap a system
;; accepting mostly only x86_64 syscalls; since it works under
;; a normal kernel and x86_64 support for mesboot is going to
;; be here soon, this won't make much sense in the future.
;;
;; The idea would be patching your own guix channel, and to let
;; `guix pull --no-substitutes` bootstrap your system. This fails
;; at the moment gnu make tries to syscall `execve` and fails (on
;; some kernels - maybe others are lucky).
;;

(define-module (kitzman packages commencement-no32)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  ;; #:use-module (guix memoization)
  #:use-module (gnu packages)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages bootstrap)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  ;; #:use-module (gnu packages c)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages gawk)
  ;; #:use-module (gnu packages bison)
  ;; #:use-module (gnu packages flex)
  ;; #:use-module (gnu packages guile)
  ;; #:use-module (gnu packages gettext)
  ;; #:use-module (gnu packages multiprecision)
  ;; #:use-module (gnu packages compression)
  #:use-module (gnu packages mes)
  ;; #:use-module (gnu packages perl)
  ;; #:use-module (gnu packages python)
  ;; #:use-module (gnu packages linux)
  ;; #:use-module (gnu packages hurd)
  ;; #:use-module (gnu packages shells)
  ;; #:use-module (gnu packages texinfo)
  ;; #:use-module (gnu packages pkg-config)
  ;; #:use-module (gnu packages rsync)
  ;; #:use-module (gnu packages xml)
  ;; #:use-module (mitzman packages)
  ;; #:use-module (guix build-system trivial)
  #:use-module (guix utils)
  #:use-module (guix memoization)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (ice-9 regex))

(define (configure-testw-patching config-files test-repl)
  `(lambda* (#:key inputs #:allow-other-keys)
     (let ((configuration-files ,config-files)
           (test-replacement ,test-repl))
       (format (current-error-port)
               "patching configuration files\n")       
       (map
        (lambda (configure-file)
          (format (current-error-port)
                  "adding ~a in ~a\n" test-replacement configure-file)
          (substitute* configure-file
            (("^SHELL=\\$\\{CONFIG_SHELL-/bin/sh\\}")
             (string-append
              "SHELL=${CONFIG_SHELL-/bin/sh}\n"
              test-replacement
              "=`$SHELL -c 'which test'`\n"))))
        configuration-files)
       (map
        (lambda (configure-file)
          (format (current-error-port)
                  "patching 'test -w' with '~a -w' in ~a\n"
                  test-replacement configure-file)
          (substitute* configure-file
            ((" test -w ")
             (string-append
              " $" test-replacement " -w "))))
        configuration-files))
     #t))

(define (configure-as_test_x-patching config-files)
  `(lambda* (#:key inputs #:allow-other-keys)
     (let ((configuration-files ,config-files))
       (format (current-error-port)
               "patching configuration files\n")       
       (map
        (lambda (configure-file)
          (format (current-error-port)
                  "modifying ~a ~a\n" "as_test_x" configure-file)
          (substitute* configure-file
            (("^as_executable_p=\\$as_test_x")
             (string-append
              "as_test_x=\"$(which test) -x\"\n"
              "as_executable_p=$as_test_x"))))
        configuration-files))
     #t))

(define (configure-testx-patching config-files)
  `(lambda* (#:key inputs #:allow-other-keys)
     (let ((configuration-files ,config-files))
       (format (current-error-port)
               "patching configuration files\n")       
       (map
        (lambda (configure-file)
          (format (current-error-port)
                  "modifying ~a ~a\n" "test -x" configure-file)
          (substitute* configure-file
            ((" test -x ")
             " $(which test) -x ")))
        configuration-files))
     #t))

(define-public binutils-mesboot0-no32
  ;; The initial (hacked) Binutils
  (package
    (inherit binutils)
    (name "binutils-mesboot0-no32")
    (version "2.14")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnu/binutils/binutils-"
                                  version ".tar.gz"))
              (sha256
               (base32
                "1w8xp7k44bkijr974x9918i4p1sw4g2fcd5mxvspkjpg38m214ds"))))
    (inputs '())
    (propagated-inputs '())
    (native-inputs ((@@ (gnu packages commencement) %boot-tcc-inputs)))
    (supported-systems '("i686-linux" "x86_64-linux"))
    (properties '((hidden? . #f)))
    (arguments
     `(#:implicit-inputs? #f
       #:guile ,%bootstrap-guile
       #:tests? #f                      ; runtest: command not found
       #:parallel-build? #f
       #:strip-binaries? #f             ; no strip yet
       #:configure-flags
       (let ((out (assoc-ref %outputs "out")))
         `("--disable-nls"
           "--disable-shared"
           "--disable-werror"
           "--build=i386-unknown-linux"
           "--host=i386-unknown-linux"
           "--target=i386-unknown-linux"
           "--with-sysroot=/"
           ,(string-append "--prefix=" out)))
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'setenv
           (lambda _
             (let* ((out (assoc-ref %outputs "out"))
                    (bash (assoc-ref %build-inputs "bash"))
                    (shell (string-append bash "/bin/bash")))
               (setenv "CONFIG_SHELL" shell)
               (setenv "SHELL" shell)
               (setenv "AR" "tcc -ar")
               (setenv "RANLIB" "true")
               (setenv "CC" "tcc -D __GLIBC_MINOR__=6")
               #t)))
         (add-after 'unpack 'scripted-patch
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* "bfd/configure"
               (("^sed -e '/SRC-POTFILES.*" all)
                "echo -e 'all:\\n\\ttrue\\n\\ninstall:\\n\\ttrue\\n' > po/Makefile\n"))
             #t))
         (replace 'configure           ; needs classic invocation of configure
           (lambda* (#:key configure-flags #:allow-other-keys)
             (format (current-error-port)
                     "running ./configure ~a\n" (string-join configure-flags))
             (apply system* "./configure" configure-flags)
             (substitute* "config.status"
               (("[.]//dev/null") "/dev/null"))
             (invoke "sh" "./config.status")))
         (add-before 'configure 'patch-testw-configure0
	   ,(configure-testw-patching
             `'("configure") "gash_test"))
	 (add-after 'configure 'patch-testw-configure1
	   ,(configure-testw-patching
             `'("bfd/configure" "binutils/configure" "configure"
                "etc/configure" "gas/configure" "gprof/configure"
                "intl/configure" "ld/configure" "libiberty/configure"
                "ltconfig" "opcodes/configure")
             "gash_test"))
         (add-after 'configure 'phail
           (lambda _ (invoke "exit" "1"))))))))

(define-public gawk-mesboot-no32
  (package
    (inherit gawk)
    (name "gawk-mesboot-no32")
    (version "3.1.8")
    (properties '((hidden? . #f)))
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnu/gawk/gawk-"
                                  version ".tar.gz"))
              (sha256
               (base32
                "03d5y7jabq7p2s7ys9alay9446mm7i5g2wvy8nlicardgb6b6ii1"))))
    (native-inputs
     (cons
      `("mesboot-headers" ,(@@ (gnu packages commencement) mesboot-headers))
      ((@@ (gnu packages commencement) %boot-mesboot0-inputs))))
    (supported-systems '("i686-linux" "x86_64-linux"))
    (inputs '())
    (propagated-inputs '())
    (arguments
     `(#:implicit-inputs? #f
       #:parallel-build? #f
       #:guile ,%bootstrap-guile
       #:configure-flags '("ac_cv_func_connect=no")
       #:make-flags '("gawk")
       #:phases
       (modify-phases %standard-phases
	 (replace 'check
           (lambda _
             (invoke "./gawk" "--version")))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin")))
               (install-file "gawk" bin)
               (symlink "gawk" (string-append bin "/awk"))
               #t)))
         (add-before 'configure 'patch-as_test_x-configure
           ,(configure-as_test_x-patching `'("configure"))))))))

(define-public gnu-make-mesboot-no32
  (package
    (inherit gnu-make)
    (name "make-mesboot-no32")
    (version "3.82")
    (properties '((hidden? . #f)))
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnu/make/make-"
                                  version ".tar.gz"))
              (sha256
               (base32
                "1rs2f9hmvy3q6zkl15jnlmnpgffm0bhw5ax0h5c7q604wqrip69x"))))
    (native-inputs ((@@ (gnu packages commencement) %boot-mesboot0-inputs)))
    (supported-systems '("i686-linux" "x86_64-linux"))
    (inputs '())
    (propagated-inputs '())
    (arguments
     `(#:implicit-inputs? #f
       #:parallel-build? #f
       #:guile ,%bootstrap-guile
       #:configure-flags '("LIBS=-lc -lnss_files -lnss_dns -lresolv")
       #:phases
       (modify-phases %standard-phases
         (replace 'check
           (lambda _
             (invoke "./make" "--version")))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin")))
               (install-file "make" bin)
               #t)))
         (add-before 'configure 'patch-as_test_x-configure
           ,(configure-as_test_x-patching `'("configure"))))))))
