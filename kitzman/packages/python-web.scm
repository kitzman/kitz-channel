;;
;; Copyright © 2021 kitzman <kitzman @ disroot.org>
;;

(define-module (kitzman packages python-web)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages check)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module ((guix licenses) #:prefix license:))

(define-public python-expiringdict
  (package
    (name "python-expiringdict")
    (version "1.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "expiringdict" version))
       (sha256
        (base32
         "0l421pczpglfwqhl1kcb7wrfxg4554p6v7yjlg4c698448ks8azy"))))
    (build-system python-build-system)
    (native-inputs
     `(("python-coverage" ,python-coverage)
       ("python-coveralls" ,python-coveralls)
       ("python-dill" ,python-dill)
       ("python-mock" ,python-mock)
       ("python-nose" ,python-nose)))
    (home-page "https://www.mailgun.com/")
    (synopsis
     "Dictionary with auto-expiring values for caching purposes")
    (description
     "Dictionary with auto-expiring values for caching purposes.")
    (license license:asl2.0)))

(define-public python-magnet2torrent
  (package
    (name "python-magnet2torrent")
    (version "1.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "magnet2torrent" version))
       (sha256
        (base32
         "022cm6w7aj46nsf6x7ry269mgx6zw4jqips81z0h610mb8ifk0x4"))))
    (build-system python-build-system)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'fix-tests
           (lambda _
             (substitute* "setup.py"
               (("aiohttp==3\\.6\\.\\*") "aiohttp>=3.6.0"))
             #t)))))
    (propagated-inputs
     `(("python-aiohttp" ,python-aiohttp)
       ("python-expiringdict" ,python-expiringdict)
       ("python-six" ,python-six)))
    (home-page
     "https://github.com/JohnDoee/magnet2torrent")
    (synopsis
     "Magnet link to torrent file converter")
    (description
     "Turn a bittorrent magnet links into a .torrent file.")
    (license license:expat)))
