;;
;; Copyright © 2021 kitzman <kitzman @ disroot.org>
;;
;; Some fonts for daily use.
;;
;; Termsyn, in three different outputs: pcf, psf and otf. Tbh the
;; otf one renders in a very ugly way, at least for me.
;;
;; And since Pango 1.44 doesn't support pcf fonts anymore - a TTF
;; font to save the day - Terminus.
;;

(define-module (kitzman packages fonts)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module (guix build-system font)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (gnu packages python)
  #:use-module (gnu packages fontutils)
  #:use-module ((guix licenses) #:prefix license:))

(define-public font-termsyn
  (package
    (name "font-termsyn")
    (version "1.8.7")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append
         "mirror://sourceforge/termsyn/termsyn-"
         version ".tar.gz"))
       (sha256
        (base32 "15vsmc3nmzl0pkgdpr2993da7p38fiw2rvcg01pwldzmpqrmkpn6"))))
    (build-system font-build-system)
    (outputs '("out"
               "psf"
               "otf"))
    (native-inputs (list fontforge))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'install 'build
           (lambda _
             (use-modules (ice-9 regex)
                          (ice-9 match))
             
             (define (pcf2 name ext)
               (invoke
                "fontforge" "-lang=ff"
                "-c"
                (string-append
                 "Open('" name "');"
                 "Generate('"
                 (basename name "pcf") ext
                 "','ttf')")))
             
             (for-each (lambda (pcf)
                         (pcf2 pcf "otf"))
                       (find-files "." "\\.pcf$"))
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((pcf (assoc-ref outputs "out"))
                    (psf (assoc-ref outputs "psf"))
                    (otf (assoc-ref outputs "otf"))
                    (pcf-font-dir (string-append pcf "/share/fonts/termsyn"))
                    (otf-font-dir (string-append otf "/share/fonts/termsyn-otf"))
                    (psf-font-dir (string-append psf "/share/kbd/consolefonts")))
               (mkdir-p pcf-font-dir)
               (mkdir-p otf-font-dir)
               (mkdir-p psf-font-dir)
               (for-each (lambda (pcf)
                           (install-file pcf pcf-font-dir))
                         (find-files "." "\\.pcf$"))
               (for-each (lambda (psfu)
                           (install-file psfu psf-font-dir))
                         (find-files "." "\\.psfu$"))
               (for-each (lambda (otf)
                           (install-file otf otf-font-dir))
                         (find-files "." "\\.otf$")))
             #t)))))
    (home-page "https://sourceforge.net/projects/termsyn/")
    (synopsis "Monospaced font based on terminus and tamsyn")
    (description
     "Monospaced font based on terminus and tamsyn.

This package contains the following outputs:
@enumerate
@item out: pcf font
@item otf: otf font
@item psf: psfu font
@end enumerate\n
")
    (license license:gpl2)))

(define-public font-terminus-ttf
  (package
    (name "font-terminus-ttf")
    (version "4.49.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://files.ax86.net/terminus-ttf/files/"
                           version "/terminus-ttf-" version ".zip"))
       (sha256
        (base32 "0fn4av5fcqvg17nwml0bvbzpjbak6fsizjdpdh6h4wqiaqkypjwz"))))
    (build-system font-build-system)
    (outputs (list "out"))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (replace 'install
           (lambda* (#:key make-flags outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (terminus-ttf-dir (string-append out "/share/fonts/terminus-ttf")))
               (mkdir-p terminus-ttf-dir)
               (for-each (lambda (ttf)
                           (install-file ttf terminus-ttf-dir))
                         (find-files "." "\\.ttf$")))
             #t)))))
    (home-page "http://terminus-font.sourceforge.net/")
    (synopsis "Simple bitmap programming font (TTF)")
    (description "Terminus Font is a clean, fixed-width bitmap font, designed
for long periods of working with computers (8 or more hours per day).")
    (license license:silofl1.1)))
